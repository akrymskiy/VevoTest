﻿CREATE TABLE [Vevo].[ArtistGenre](
	[artistId] [int] NOT NULL,
	[genreId] [smallint] NOT NULL,
 CONSTRAINT [PK_ArtistGenre] PRIMARY KEY CLUSTERED 
(
	[artistId] ASC,
	[genreId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Vevo].[ArtistGenre]  WITH CHECK ADD  CONSTRAINT [FK_ArtistGenre_Artist] FOREIGN KEY([artistId])
REFERENCES [Vevo].[Artist] ([id])
ON DELETE CASCADE
GO

ALTER TABLE [Vevo].[ArtistGenre] CHECK CONSTRAINT [FK_ArtistGenre_Artist]
GO
ALTER TABLE [Vevo].[ArtistGenre]  WITH CHECK ADD  CONSTRAINT [FK_ArtistGenre_Genre] FOREIGN KEY([genreId])
REFERENCES [Vevo].[Genre] ([id])
GO

ALTER TABLE [Vevo].[ArtistGenre] CHECK CONSTRAINT [FK_ArtistGenre_Genre]