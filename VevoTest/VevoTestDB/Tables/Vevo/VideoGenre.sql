﻿CREATE TABLE [Vevo].[VideoGenre](
	[videoId] [int] NOT NULL,
	[genreId] [smallint] NOT NULL,
	[created] [datetime] NOT NULL CONSTRAINT [DF_VideoGenre_created]  DEFAULT (getutcdate()),
	[modified] [datetime] NULL,
 CONSTRAINT [PK_VideoGenre] PRIMARY KEY CLUSTERED 
(
	[videoId] ASC,
	[genreId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Vevo].[VideoGenre]  WITH CHECK ADD  CONSTRAINT [FK_VideoGenre_Genre] FOREIGN KEY([genreId])
REFERENCES [Vevo].[Genre] ([id])
GO

ALTER TABLE [Vevo].[VideoGenre] CHECK CONSTRAINT [FK_VideoGenre_Genre]
GO
ALTER TABLE [Vevo].[VideoGenre]  WITH CHECK ADD  CONSTRAINT [FK_VideoGenre_Video] FOREIGN KEY([videoId])
REFERENCES [Vevo].[Video] ([id])
ON DELETE CASCADE
GO

ALTER TABLE [Vevo].[VideoGenre] CHECK CONSTRAINT [FK_VideoGenre_Video]
GO
/****** Object:  Index [NK_VideoGenre_genreId]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE NONCLUSTERED INDEX [NK_VideoGenre_genreId] ON [Vevo].[VideoGenre]
(
	[genreId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]