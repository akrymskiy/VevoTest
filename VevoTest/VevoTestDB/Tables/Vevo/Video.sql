﻿CREATE TABLE [Vevo].[Video](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[isrc] [char](12) NOT NULL,
	[title] [nvarchar](300) NOT NULL,
	[videoYear] [smallint] NULL,
	[copyrightLine] [nvarchar](1000) NULL,
	[shortUrl] [varchar](100) NULL,
	[thumbnail] [varchar](500) NULL,
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[duration] [time](7) NULL CONSTRAINT [DF_Video_duration]  DEFAULT ('00:00:00'),
	[flags] [int] NULL CONSTRAINT [DF_Video_flags]  DEFAULT ((0)),
	[created] [datetime] NOT NULL CONSTRAINT [DF_Video_created]  DEFAULT (getutcdate()),
	[modified] [datetime] NULL,
	[viewsTotal] [bigint] NOT NULL CONSTRAINT [DF_Video_viewsTotal]  DEFAULT ((0)),
	[viewsLastWeek] [bigint] NOT NULL CONSTRAINT [DF_Video_viewsLastWeek]  DEFAULT ((0)),
	[viewsLastMonth] [bigint] NOT NULL CONSTRAINT [DF_Video_viewsLastMonth]  DEFAULT ((0)),
	[viewsLastDay] [bigint] NOT NULL CONSTRAINT [DF_Video_viewsLastDay]  DEFAULT ((0)),
	[favoriteLastDay] [bigint] NOT NULL CONSTRAINT [DF_Video_favoriteLastDay]  DEFAULT ((0)),
	[favoriteLastWeek] [bigint] NOT NULL CONSTRAINT [DF_Video_favoriteLastWeek]  DEFAULT ((0)),
	[favoriteLastMonth] [bigint] NOT NULL CONSTRAINT [DF_Video_favoriteLastMonth]  DEFAULT ((0)),
	[favoriteTotal] [bigint] NOT NULL CONSTRAINT [DF_Video_favoriteTotal]  DEFAULT ((0)),
 CONSTRAINT [PK_Video] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [NK_Video_flags_id]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE NONCLUSTERED INDEX [NK_Video_flags_id] ON [Vevo].[Video]
(
	[flags] ASC
)
INCLUDE ( 	[id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NK_Video_startDate_endDate_flags_id_viewsLastWeek]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE NONCLUSTERED INDEX [NK_Video_startDate_endDate_flags_id_viewsLastWeek] ON [Vevo].[Video]
(
	[startDate] ASC,
	[endDate] ASC
)
INCLUDE ( 	[id],
	[flags],
	[viewsLastWeek]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NK_Video_title]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE NONCLUSTERED INDEX [NK_Video_title] ON [Vevo].[Video]
(
	[title] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UK_Video_isrc]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UK_Video_isrc] ON [Vevo].[Video]
(
	[isrc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]