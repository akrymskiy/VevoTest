﻿CREATE TABLE [Vevo].[ArtistVideoRole](
	[id] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[artistId] [int] NOT NULL,
	[videoId] [int] NOT NULL,
	[roleTypeId] [tinyint] NOT NULL,
	[sort] [tinyint] NOT NULL CONSTRAINT [DF_ArtistVideoRole_sort]  DEFAULT ((0)),
	[created] [datetime] NOT NULL CONSTRAINT [DF_ArtistVideoRole_created]  DEFAULT (getutcdate()),
	[modified] [datetime] NULL,
 CONSTRAINT [PK_ArtistVideoRole] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [Vevo].[ArtistVideoRole]  WITH CHECK ADD  CONSTRAINT [FK_ArtistVideoRole_Artist] FOREIGN KEY([artistId])
REFERENCES [Vevo].[Artist] ([id])
ON DELETE CASCADE
GO

ALTER TABLE [Vevo].[ArtistVideoRole] CHECK CONSTRAINT [FK_ArtistVideoRole_Artist]
GO
ALTER TABLE [Vevo].[ArtistVideoRole]  WITH CHECK ADD  CONSTRAINT [FK_ArtistVideoRole_RoleType] FOREIGN KEY([roleTypeId])
REFERENCES [Reference].[ArtistRoleType] ([id])
GO

ALTER TABLE [Vevo].[ArtistVideoRole] CHECK CONSTRAINT [FK_ArtistVideoRole_RoleType]
GO
ALTER TABLE [Vevo].[ArtistVideoRole]  WITH CHECK ADD  CONSTRAINT [FK_ArtistVideoRole_Video] FOREIGN KEY([videoId])
REFERENCES [Vevo].[Video] ([id])
ON DELETE CASCADE
GO

ALTER TABLE [Vevo].[ArtistVideoRole] CHECK CONSTRAINT [FK_ArtistVideoRole_Video]
GO
/****** Object:  Index [NK_ArtistVideoRole_artistId_videoId]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE NONCLUSTERED INDEX [NK_ArtistVideoRole_artistId_videoId] ON [Vevo].[ArtistVideoRole]
(
	[artistId] ASC,
	[videoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NK_ArtistVideoRole_artistId_videoId_roleTypeId]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE NONCLUSTERED INDEX [NK_ArtistVideoRole_artistId_videoId_roleTypeId] ON [Vevo].[ArtistVideoRole]
(
	[artistId] ASC,
	[videoId] ASC,
	[roleTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [NK_ArtistVideoRole_videoId_artistId_roleTypeId_sort]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE NONCLUSTERED INDEX [NK_ArtistVideoRole_videoId_artistId_roleTypeId_sort] ON [Vevo].[ArtistVideoRole]
(
	[videoId] ASC
)
INCLUDE ( 	[artistId],
	[roleTypeId],
	[sort]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [UK_ArtistVideoRole_videoId_artistId_roleTypeId]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UK_ArtistVideoRole_videoId_artistId_roleTypeId] ON [Vevo].[ArtistVideoRole]
(
	[videoId] ASC,
	[artistId] ASC,
	[roleTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]