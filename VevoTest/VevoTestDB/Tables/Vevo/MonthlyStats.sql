﻿create table [Vevo].[MonthlyStats]
(
	[ReportDate] date not null,
	[artistId] int not null,
	[genreId] smallint not null,
	[VideoCount] int not null,
	[TotalGenreViews] int not null,
	[GenreProportion] numeric(5, 2),
	[RefreshDateTime] datetime not null constraint DF_RefreshDateTime default getdate(),
	constraint PK_MonthlyStats primary key clustered ([ReportDate], [artistId], [genreId])
)
