﻿/****** Object:  UserDefinedTableType [Util].[StringTable]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE TYPE [Util].[StringTable] AS TABLE(
	[value] [varchar](500) NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[value] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)