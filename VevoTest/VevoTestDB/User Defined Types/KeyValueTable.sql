﻿/****** Object:  UserDefinedTableType [Util].[KeyValueTable]    Script Date: 6/10/2015 3:09:19 AM ******/
CREATE TYPE [Util].[KeyValueTable] AS TABLE(
	[key] [varchar](500) NOT NULL,
	[value] [varchar](500) NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[key] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)