﻿/****** Object:  UserDefinedTableType [Util].[IdTable]    Script Date: 6/10/2015 3:09:18 AM ******/
CREATE TYPE [Util].[IdTable] AS TABLE(
	[id] [int] NOT NULL,
	PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)