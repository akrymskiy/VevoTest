﻿create function [Util].[SplitString] 
(
    @str		nvarchar(max), 
    @separator	varchar(10)
)
returns @items table (id int identity(1, 1), item nvarchar(4000))
with schemabinding
as
begin
   declare @ll int = len(@str) + 1, @ld int = len(@separator);
 
   with a as
   (
       select
           [start] = 1,
           [end]   = coalesce(nullif(charindex(@separator, 
                       @str, @ld), 0), @ll),
           [value] = substring(@str, 1, 
                     coalesce(nullif(charindex(@separator, 
                       @str, @ld), 0), @ll) - 1)
       union all
       select
           [start] = convert(int, [end]) + @ld,
           [end]   = coalesce(nullif(charindex(@separator, 
                       @str, [end] + @ld), 0), @ll),
           [value] = substring(@str, [end] + @ld, 
                     coalesce(nullif(charindex(@separator, 
                       @str, [end] + @ld), 0), @ll)-[end]-@ld)
       from a
       where [end] < @ll
   )
   insert @items select [value]
   from a
   where len([value]) > 0
   option (maxrecursion 0);
 
   return;
end